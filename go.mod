module gitlab.com/ziik/mado-vulkan-example

go 1.21

require (
	github.com/kanryu/mado v0.0.0-20240315045106-3400d7f7d662
	github.com/vulkan-go/vulkan v0.0.0-20221209234627-c0a353ae26c8
	github.com/xlab/closer v0.0.0-20190328110542-03326addb7c2
	github.com/xlab/linmath v0.0.0-20220922225318-40b6290c3b40
)

require (
	gioui.org/cpu v0.0.0-20210817075930-8d6a761490d2 // indirect
	gioui.org/shader v1.0.8 // indirect
	github.com/go-text/typesetting v0.0.0-20230803102845-24e03d8b5372 // indirect
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9 // indirect
	golang.org/x/exp/shiny v0.0.0-20220906200021-fcb1a314c389 // indirect
	golang.org/x/image v0.7.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
